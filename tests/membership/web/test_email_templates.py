import json
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import SUPER_USER_FIRST_NAME, SUPER_USER_LAST_NAME, SUPER_USER_EMAIL
from membership.database.base import engine, metadata, Session
from membership.schemas.rest import EmailTemplateRest
from membership.database.models import EmailTemplate, InterestTopic, Member, Role
from membership.web.base_app import app
from tests.flask_utils import delete_json, get_json, post_json, put_json


class TestEmailTemplates:
    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True
        session = Session()
        # set up for auth
        admin_member = Member(
            first_name=SUPER_USER_FIRST_NAME,
            last_name=SUPER_USER_LAST_NAME,
            email_address=SUPER_USER_EMAIL,
        )
        session.add(admin_member)
        role = Role(
            member=admin_member,
            role='admin',
        )
        session.add(role)
        self.topic1 = InterestTopic(name='Topic 1')
        session.add(self.topic1)
        self.topic2 = InterestTopic(name='Topic 2')
        session.add(self.topic2)
        self.template1 = EmailTemplate(name='Template 1', topic=self.topic1)
        session.add(self.template1)
        self.template2 = EmailTemplate(name='Template 2')
        session.add(self.template2)
        session.commit()
        self.session = session

    def teardown(self):
        metadata.drop_all(engine)

    def test_find_by_id(self):
        response = get_json(self.app, '/email_template/1')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == EmailTemplateRest.format(self.template1)

    def test_not_found_by_id(self):
        response = get_json(self.app, '/email_template/1337')
        assert response.status_code == 404

    def test_list(self):
        response = get_json(self.app, '/email_templates')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == [
            EmailTemplateRest.format(template) for template in (
                self.template1,
                self.template2,
            )
        ]

    def test_create(self):
        response = post_json(self.app, '/email_template', {
            "name": "Template 3",
            "topic_id": self.topic1.id,
        })
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result['name'] == "Template 3"
        assert result['topic_id'] == self.topic1.id
        assert 'last_updated' in result

    def test_update(self):
        response = put_json(self.app, '/email_template/1', {
            "subject": "Template 1",
            "topic_id": self.topic2.id,
        })
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result['subject'] == "Template 1"
        assert result['topic_id'] == self.topic2.id

    def test_update_topic_not_found(self):
        response = put_json(self.app, '/email_template/1', {
            "subject": "Template 1",
            "topic_id": 1337,
        })
        assert response.status_code == 404

    def test_delete(self):
        response = delete_json(self.app, '/email_template/1')
        assert response.status_code == 200
        response = get_json(self.app, '/email_template/1')
        assert response.status_code == 404
