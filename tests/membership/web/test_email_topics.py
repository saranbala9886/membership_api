import json
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import EMAIL_TOPICS_ADD_MEMBER_TOKEN
from config import SUPER_USER_EMAIL
from config import SUPER_USER_FIRST_NAME
from config import SUPER_USER_LAST_NAME
from membership.database.base import engine, metadata, Session
from membership.database.models import InterestTopic, Member, Meeting, Role, EmailTemplate
from membership.models.authz import AuthContext
from membership.services import \
    AttendeeService, InterestTopicsService, MemberService, MeetingService
from membership.services.eligibility import SanFranciscoEligibilityService
from membership.repos import MeetingRepo
from membership.web.base_app import app
from tests.flask_utils import get_json, post_json


interest_topics_service = InterestTopicsService
meeting_service = MeetingService()
meeting_repository = MeetingRepo(Meeting)
attendee_service = AttendeeService()
eligibility_service = SanFranciscoEligibilityService(
    meetings=meeting_repository,
    attendee_service=attendee_service
)
member_service = MemberService(eligibility_service)


class TestEmailTopics:
    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True
        session = Session()
        # set up for auth
        admin_member = Member(
            first_name=SUPER_USER_FIRST_NAME,
            last_name=SUPER_USER_LAST_NAME,
            email_address=SUPER_USER_EMAIL,
        )
        session.add(admin_member)
        role = Role(
            member=admin_member,
            role='admin',
        )
        session.add(role)
        topic1 = InterestTopic(name='SocFem')
        session.add(topic1)
        topic2 = InterestTopic(name='Tech')
        session.add(topic2)

        template = EmailTemplate(
            name='Template',
            subject='Subject',
            body='Body',
        )
        session.add(template)
        session.commit()
        self.session = session

    def teardown(self):
        metadata.drop_all(engine)

    def test_list_topics(self):
        response = get_json(self.app, '/email_topics')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == [
            {
                'id': 1,
                'name': 'SocFem',
            },
            {
                'id': 2,
                'name': 'Tech',
            }
        ]

    def test_add_interests_existing_member(self):
        payload = {
            'topics': 'SocFem, Tech, Housing',
            'email': SUPER_USER_EMAIL,
            'token': EMAIL_TOPICS_ADD_MEMBER_TOKEN,
        }
        response = self.app.post(
            '/email_topics/add_member',
            data=payload,
        )
        assert response.status_code == 200

        session = Session()
        member = member_service.find_by_email(SUPER_USER_EMAIL, session)
        assert member is not None

        ctx = AuthContext(member, session)
        topics = interest_topics_service.list_member_interest_topics(ctx, member)
        assert len(topics) == 3

    def test_email_topic_member_count(self):
        # Verify counts are zero at the start.
        response = get_json(self.app, '/email_topics/member_count/SocFem')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == 0

        response = get_json(self.app, '/email_topics/member_count/Tech')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == 0

        # Add a member to SocFem and Tech.
        payload = {
            'topics': 'SocFem, Tech',
            'email': ' debs1855@gmail.com',
            'first_name': 'Eugene ',
            'last_name': 'Debs',
            'token': EMAIL_TOPICS_ADD_MEMBER_TOKEN,
        }
        response = self.app.post(
            '/email_topics/add_member',
            data=payload,
        )
        assert response.status_code == 200

        # Verify counts are updated.
        response = get_json(self.app, '/email_topics/member_count/SocFem')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == 1

        response = get_json(self.app, '/email_topics/member_count/Tech')
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result == 1

    def test_email_topic_member_count_unknown_topic(self):
        response = get_json(self.app, '/email_topics/member_count/FakeTopic')
        assert response.status_code == 404

    def test_add_interests_new_member(self):
        payload = {
            'topics': 'SocFem, Tech, Housing',
            'email': ' debs.1855@gmail.com',
            'first_name': 'Eugene ',
            'last_name': 'Debs',
            'token': EMAIL_TOPICS_ADD_MEMBER_TOKEN,
        }
        response = self.app.post(
            '/email_topics/add_member',
            data=payload,
        )
        assert response.status_code == 200

        session = Session()
        member = member_service.find_by_email('debs1855@gmail.com', session)
        assert member is not None
        assert member.first_name == 'Eugene'
        assert member.last_name == 'Debs'
        assert member.email_address == 'debs.1855@gmail.com'
        assert member.normalized_email == 'debs1855@gmail.com'
        assert member.date_created is not None

        ctx = AuthContext(member, session)
        topics = interest_topics_service.list_member_interest_topics(ctx, member)
        assert len(topics) == 3

    def test_add_interests_no_email(self):
        payload = {
            'topics': 'Tech',
            'email': '',
            'first_name': 'Eugene',
            'last_name': 'Debs',
            'token': EMAIL_TOPICS_ADD_MEMBER_TOKEN,
        }
        response = self.app.post(
            '/email_topics/add_member',
            data=payload,
        )
        assert response.status_code == 400

    def test_add_interests_new_member_empty_topics(self):
        payload = {
            'topics': '',
            'email': 'debs1855@gmail.com',
            'first_name': 'Eugene',
            'last_name': 'Debs',
            'token': EMAIL_TOPICS_ADD_MEMBER_TOKEN,
        }
        response = self.app.post(
            '/email_topics/add_member',
            data=payload,
        )
        assert response.status_code == 200

        session = Session()
        member = member_service.find_by_email('debs1855@gmail.com', session)
        assert member is not None

        ctx = AuthContext(member, session)
        topics = interest_topics_service.list_member_interest_topics(ctx, member)
        assert topics == []

    def test_send_email(self):
        payload = {
            'topic': '1',
            'sending_address': 'tech@dsasf.org',
            'template_id': 1,
        }
        response = post_json(self.app, '/email_topics/send', payload)
        assert response.status_code == 200

    def test_send_email_topic_not_found(self):
        payload = {
            'topic_id': '2',
            'sending_address': 'tech@dsasf.org',
            'template_id': 1337,
        }
        response = post_json(self.app, '/email_topics/send', payload)
        assert response.status_code == 404
