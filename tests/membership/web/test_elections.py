import json
import datetime

from sqlalchemy import desc

from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import SUPER_USER_EMAIL
from membership.database.base import engine, metadata, Session
from membership.database.models import Candidate, Election, EligibleVoter, Member, \
    Ranking, Role, Sponsor, Vote
from membership.services import ElectionService
from membership.web.base_app import app
from tests.flask_utils import get_json, patch_json, post_json, put_json


class TestElections:
    current_time_millis = None

    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

        session = Session()

        # set up for auth
        admin_member = Member(
            first_name='Joe',
            last_name='Schmoe',
            email_address=SUPER_USER_EMAIL,
        )
        session.add(admin_member)

        role = Role(
            member=admin_member,
            role='admin',
        )
        session.add(role)

        current_datetime = datetime.datetime.utcnow() - datetime.datetime(1970, 1, 1)
        self.current_time_millis = int(current_datetime.total_seconds() * 1000)

        election = Election(
            name='Election 1',
            number_winners=1,
            voting_begins_epoch_millis=self.current_time_millis,
            voting_ends_epoch_millis=(self.current_time_millis + (60 * 60 * 1000)),
            description='Election 1 description',
            description_img='http://s3.amazonaws.com/election1.jpg',
            author='Election 1 author',
        )
        session.add(election)

        candidate1 = Candidate()
        candidate1.name = 'Candidate 1'
        candidate1.election = election
        session.add(candidate1)

        candidate2 = Candidate()
        candidate2.name = 'Candidate 2'
        candidate2.election = election
        session.add(candidate2)

        sponsor1 = Sponsor()
        sponsor1.name = 'Sponsor 1'
        sponsor1.election = election
        session.add(sponsor1)

        sponsor2 = Sponsor()
        sponsor2.name = 'Sponsor 2'
        sponsor2.election = election
        session.add(sponsor2)

        voting_member1 = Member(
            first_name='Emma',
            last_name='Goldman',
            email_address='emma.goldman@riseup.net',
        )
        session.add(voting_member1)

        voting_member2 = Member(
            first_name='Eugene',
            last_name='Debs',
            email_address='debs1855@gmail.com',
        )
        session.add(voting_member2)

        voting_member3 = Member(
            first_name='W.E.B.',
            last_name='Du Bois',
            email_address='du.bois@gmail.com',
        )
        session.add(voting_member3)

        eligible_voter1 = EligibleVoter(member=voting_member1, election=election, voted=True)
        session.add(eligible_voter1)

        eligible_voter2 = EligibleVoter(member=voting_member2, election=election, voted=False)
        session.add(eligible_voter2)

        eligible_voter3 = EligibleVoter(member=admin_member, election=election, voted=False)
        session.add(eligible_voter3)

        vote1 = Vote()
        vote1.vote_key = 12345
        vote1.election = election

        ranking1 = Ranking()
        ranking1.rank = 1
        ranking1.candidate = candidate1
        ranking1.vote = vote1
        session.add(ranking1)

        ranking2 = Ranking()
        ranking2.rank = 2
        ranking2.candidate = candidate2
        ranking2.vote = vote1
        session.add(ranking2)

        vote1.ranking = [
            ranking1,
            ranking2,
        ]
        session.add(vote1)

        vote2 = Vote()
        vote2.vote_key = 54321
        vote2.election = election
        session.add(vote2)

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def test_election_list(self):
        response = get_json(self.app, '/election/list')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == [{
            'id': 1,
            'name': 'Election 1',
            'status': 'draft',
            'voting_begins_epoch_millis': self.current_time_millis,
            'voting_ends_epoch_millis': (self.current_time_millis + (60 * 60 * 1000))
        }]

    def test_election_get_by_id(self):
        response = get_json(self.app, '/election?id=1')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'id': 1,
            'name': 'Election 1',
            'number_winners': 1,
            'candidates': [
                {'id': 1, 'name': 'Candidate 1', 'image_url': None},
                {'id': 2, 'name': 'Candidate 2', 'image_url': None},
            ],
            'sponsors': [
                {'id': 1, 'name': 'Sponsor 1'},
                {'id': 2, 'name': 'Sponsor 2'},
            ],
            'status': 'draft',
            'transitions': ['publish', 'cancel'],
            'voting_begins_epoch_millis': self.current_time_millis,
            'voting_ends_epoch_millis': (self.current_time_millis + (60 * 60 * 1000)),
            'description': 'Election 1 description',
            'description_img': 'http://s3.amazonaws.com/election1.jpg',
            'author': 'Election 1 author'
        }

    def test_election_get_by_id_not_found(self):
        response = get_json(self.app, '/election?id=1337')
        assert response.status_code == 404

    def test_election_get_by_id_bad_request(self):
        response = get_json(self.app, '/election')
        assert response.status_code == 400

    def test_change_election_state(self):
        response = put_json(self.app, '/election/1/state/publish')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'status': 'success',
            'election_status': 'published',
            'transitions': ['finalize', 'cancel'],
        }

    def test_change_election_state_id_not_found(self):
        response = put_json(self.app, '/election/1337/state/publish')
        assert response.status_code == 404

    def test_change_election_state_invalid_state_transition(self):
        response = put_json(self.app, '/election/1/state/bedazzle')
        assert response.status_code == 400

    def test_add_election(self):
        payload = {
            'name': 'Mock Election',
            'number_winners': 1,
            'start_time': '2016-11-08T16:00:00.000Z',
            'end_time': '2016-11-09T04:00:00.000Z',
            'description': 'Mock Election description',
            'description_img': 'http://mockelection.com/image.jpg',
            'author': 'Mock Election author',
            'candidate_list': [
                'Emma Goldman',
                'Eugene Debs',
            ],
            'sponsor_list': [
                'Sponsor 1',
                'Sponsor 2',
            ]
        }
        response = post_json(self.app, '/election', payload=payload)
        assert response.status_code == 200

        session = Session()
        election = session.query(Election).order_by(desc(Election.id)).first()
        assert election.name == 'Mock Election'
        assert election.voting_begins_epoch_millis == 1478620800000
        assert election.voting_ends_epoch_millis == 1478664000000
        assert election.number_winners == 1
        assert election.description == 'Mock Election description'
        assert election.description_img == 'http://mockelection.com/image.jpg'
        assert election.author == 'Mock Election author'
        assert len(election.candidates) == 2
        assert len(election.sponsors) == 2

    def test_add_duplicate_election(self):
        payload = {
            'name': 'Election 1',
            'number_winners': 1,
            'candidate_list': [
                'Emma Goldman',
                'Eugene Debs',
            ]
        }
        response = post_json(self.app, '/election', payload=payload)
        assert response.status_code == 400

        session = Session()
        assert session.query(Election).count() == 1

    def test_update_election(self):
        payload = {
            'start_time': None,
            'end_time': None,
        }
        response = patch_json(self.app, '/election/1', payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'status': 'success',
            'election': {
                'id': 1,
                'name': 'Election 1',
                'number_winners': 1,
                'candidates': [
                    {'id': 1, 'name': 'Candidate 1', 'image_url': None},
                    {'id': 2, 'name': 'Candidate 2', 'image_url': None},
                ],
                'sponsors': [
                    {'id': 1, 'name': 'Sponsor 1'},
                    {'id': 2, 'name': 'Sponsor 2'},
                ],
                'status': 'draft',
                'transitions': ['publish', 'cancel'],
                'voting_begins_epoch_millis': None,
                'voting_ends_epoch_millis': None,
                'description': 'Election 1 description',
                'description_img': 'http://s3.amazonaws.com/election1.jpg',
                'author': 'Election 1 author'
            }
        }

    def test_update_election_not_found(self):
        payload = {
            'start_time': None,
            'end_time': None,
        }
        response = patch_json(self.app, '/election/55', payload)
        assert response.status_code == 404

    def test_update_election_published(self):
        put_json(self.app, '/election/1/state/publish')

        payload = {
            'start_time': None,
            'end_time': None,
        }
        response = patch_json(self.app, '/election/1', payload)
        assert response.status_code == 400

    def test_get_eligible(self):
        response = get_json(self.app, '/election/eligible/list?election_id=1')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            '1': {'name': 'Joe Schmoe', 'email_address': SUPER_USER_EMAIL},
            '2': {'name': 'Emma Goldman', 'email_address': 'emma.goldman@riseup.net'},
            '3': {'name': 'Eugene Debs', 'email_address': 'debs1855@gmail.com'},
        }

    def test_get_eligible_not_found(self):
        response = get_json(self.app, '/election/eligible/list?election_id=1337')
        assert response.status_code == 404

    def test_get_eligible_bad_request(self):
        response = get_json(self.app, '/election/eligible/list')
        assert response.status_code == 400

    def test_add_voter(self):
        payload = {'election_id': 1, 'member_id': 4}
        response = post_json(self.app, '/election/voter', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success'}

        session = Session()
        eligible_voter = session.query(EligibleVoter).order_by(desc(EligibleVoter.id)).first()
        assert eligible_voter.election.id == 1
        assert eligible_voter.member.id == 4

    def test_add_voter_duplicate(self):
        payload = {'election_id': 1, 'member_id': 1}
        response = post_json(self.app, '/election/voter', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success'}

    def test_add_voter_missing_election_id(self):
        response = post_json(self.app, '/election/voter', payload={'member_id': 4})
        assert response.status_code == 400

    def test_get_vote(self):
        response = get_json(self.app, '/election/1/vote/12345')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'election_id': 1,
            'ballot_key': 12345,
            'rankings': [1, 2],
        }

    def test_get_vote_election_not_found(self):
        response = get_json(self.app, '/election/1337/vote/12345')
        assert response.status_code == 404

    def test_get_vote_ballot_not_found(self):
        response = get_json(self.app, '/election/1/vote/1')
        assert response.status_code == 404

    def test_issue_ballot(self):
        payload = {'election_id': 1, 'member_id': 3}
        response = post_json(self.app, '/ballot/issue', payload=payload)
        assert response.status_code == 200

    def test_issue_ballot_missing_election_id_param(self):
        response = post_json(self.app, '/ballot/issue', payload={'member_id': 3})
        assert response.status_code == 400

    def test_issue_ballot_missing_member_id_param(self):
        response = post_json(self.app, '/ballot/issue', payload={'election_id': 1})
        assert response.status_code == 400

    def test_issue_ballot_already_voted(self):
        payload = {'election_id': 1, 'member_id': 2}
        response = post_json(self.app, '/ballot/issue', payload=payload)
        assert response.status_code == 400

    def test_issue_ballot_not_eligible(self):
        payload = {'election_id': 1, 'member_id': 4}
        response = post_json(self.app, '/ballot/issue', payload=payload)
        assert response.status_code == 400

    def test_add_paper_ballots(self):
        payload = {'election_id': 1, 'number_ballots': 3}
        response = post_json(self.app, '/ballot/claim', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert type(result) == list
        assert len(result) == 3
        assert len(set(result)) == 3

    def test_add_paper_ballots_missing_election_id_param(self):
        response = post_json(self.app, '/ballot/claim', payload={'number_ballots': 3})
        assert response.status_code == 400

    def test_add_paper_ballots_missing_number_ballots_param(self):
        response = post_json(self.app, '/ballot/claim', payload={'election_id': 1})
        assert response.status_code == 400

    def test_submit_paper_vote(self):
        payload = {
            'election_id': 1,
            'ballot_key': 54321,
        }
        response = post_json(self.app, '/vote/paper', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'new'}

    def test_submit_paper_vote_election_not_found(self):
        payload = {
            'election_id': 2,
            'ballot_key': 54321,
        }
        response = post_json(self.app, '/vote/paper', payload=payload)
        assert response.status_code == 404

    def test_submit_paper_vote_election_closed(self):
        session = Session()
        service = ElectionService()
        service.transition_election_as(session, 'publish', 1)
        service.transition_election_as(session, 'finalize', 1)

        payload = {
            'election_id': 1,
            'ballot_key': 54321,
        }
        response = post_json(self.app, '/vote/paper', payload=payload)
        assert response.status_code == 400

    def test_submit_paper_vote_ballot_not_found(self):
        payload = {
            'election_id': 1,
            'ballot_key': 99999,
        }
        response = post_json(self.app, '/vote/paper', payload=payload)
        assert response.status_code == 404

    def test_submit_paper_vote_override(self):
        payload = {
            'election_id': 1,
            'ballot_key': 12345,
            'rankings': [2, 1],
            'override': True,
        }
        response = post_json(self.app, '/vote/paper', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'new'}

        session = Session()
        vote = session.query(Vote).filter_by(election_id=1, vote_key=12345).one_or_none()
        assert vote.ranking[0].candidate.name == 'Candidate 2'
        assert vote.ranking[1].candidate.name == 'Candidate 1'

    def test_submit_paper_vote_match(self):
        payload = {
            'election_id': 1,
            'ballot_key': 12345,
            'rankings': [1, 2],
        }
        response = post_json(self.app, '/vote/paper', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'match'}

    def test_submit_paper_vote_mismatch(self):
        payload = {
            'election_id': 1,
            'ballot_key': 12345,
            'rankings': [2, 1],
        }
        response = post_json(self.app, '/vote/paper', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'mismatch'}

    def test_submit_vote(self):
        payload = {
            'election_id': 1,
            'rankings': [],
        }
        response = post_json(self.app, '/vote', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert list(result) == ['ballot_id']

        # ballot_id should be a random 6 digit number
        ballot_id = result['ballot_id']
        assert 99999 < ballot_id < 1000000

    def test_submit_vote_token(self):
        response = get_json(self.app, '/election/1/generate_token/1')
        assert response.status_code == 200
        result = json.loads(response.data)

        payload = {
            'auth_token_id': result['auth_token_id'],
            'election_id': 1,
            'rankings': [],
        }
        response = post_json(self.app, '/vote', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert list(result) == ['ballot_id']

        # ballot_id should be a random 6 digit number
        ballot_id = result['ballot_id']
        assert 99999 < ballot_id < 1000000

    def test_submit_vote_election_not_found(self):
        payload = {
            'election_id': 1337,
            'rankings': [],
        }
        response = post_json(self.app, '/vote', payload=payload)
        assert response.status_code == 404

    def test_submit_vote_missing_election_id(self):
        payload = {'rankings': []}
        response = post_json(self.app, '/vote', payload=payload)
        assert response.status_code == 400

    def test_submit_vote_too_early(self):
        session = Session()
        election = session.query(Election).get(1)
        election.voting_begins_epoch_millis = election.voting_ends_epoch_millis
        session.add(election)
        session.commit()

        payload = {
            'election_id': 1,
            'rankings': [],
        }
        response = post_json(self.app, '/vote', payload=payload)
        assert response.status_code == 400

    def test_submit_vote_too_late(self):
        session = Session()
        election = session.query(Election).get(1)
        election.voting_ends_epoch_millis = election.voting_begins_epoch_millis
        session.add(election)
        session.commit()

        payload = {
            'election_id': 1,
            'rankings': [],
        }
        response = post_json(self.app, '/vote', payload=payload)
        assert response.status_code == 400

    def test_submit_vote_twice(self):
        payload = {
            'election_id': 1,
            'rankings': [],
        }
        response = post_json(self.app, '/vote', payload=payload)
        assert response.status_code == 200

        response = post_json(self.app, '/vote', payload=payload)
        assert response.status_code == 400

    def test_submit_vote_not_eligible(self):
        session = Session()
        session.query(EligibleVoter).delete()
        session.commit()

        payload = {
            'election_id': 1,
            'rankings': [],
        }
        response = post_json(self.app, '/vote', payload=payload)
        assert response.status_code == 400

    def test_election_count(self):
        put_json(self.app, '/election/1/state/publish')
        put_json(self.app, '/election/1/state/finalize')
        response = get_json(self.app, '/election/count?id=1')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'ballot_count': 1,
            'quota': 1,
            'candidates': {
                '1': {
                    'name': 'Candidate 1',
                    'image_url': None,
                },
                '2': {
                    'name': 'Candidate 2',
                    'image_url': None,
                },
            },
            'winners': [
                {
                    'candidate': 1,
                    'round': 1,
                },
            ],
            'round_information': [
                {
                    'transfer_type': 'excess',
                    'transfer_source': 1,
                    'votes': {
                        '1': {'starting_votes': 1.0, 'votes_received': 0.0},
                        '2': {'starting_votes': 0.0, 'votes_received': 0.0},
                    },
                },
            ]
        }

    def test_election_count_election_not_final(self):
        response = get_json(self.app, '/election/count?id=1')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'ballot_count': 1,
            'quota': 1,
            'candidates': {
                '1': {
                    'name': 'Candidate 1',
                    'image_url': None,
                },
                '2': {
                    'name': 'Candidate 2',
                    'image_url': None,
                },
            },
            'winners': [],
            'round_information': []
        }

    def test_election_count_missing_election_id(self):
        response = get_json(self.app, '/election/count')
        assert response.status_code == 400

    def test_election_count_election_not_found(self):
        response = get_json(self.app, '/election/count?id=1337')
        assert response.status_code == 404
