from unittest.mock import patch

import json
from datetime import datetime, timezone

from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import SUPER_USER_EMAIL
from membership.database.base import engine, metadata, Session
from membership.database.models import Attendee, Committee, Meeting, Member, Role
from membership.web.base_app import app
from tests.flask_utils import get_json, patch_json, post_json, delete_json


class TestWebMeetings:
    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

        # set up for auth
        session = Session()
        m = Member()
        m.email_address = SUPER_USER_EMAIL
        session.add(m)
        role = Role()
        role.member = m
        role.role = 'admin'
        session.add(role)
        committee1 = Committee(id=1, name='Committee 1')

        meeting1 = Meeting(
            id=1,
            short_id=1,
            name='Meeting 1',
            start_time=datetime(2017, 1, 1, 0),
            end_time=datetime(2017, 1, 1, 1),
        )
        meeting2 = Meeting(
            id=2,
            short_id=2,
            name='Meeting 2',
            start_time=datetime(2017, 2, 1, 0),
            end_time=datetime(2017, 2, 1, 1),
        )
        meeting3 = Meeting(
            id=3,
            short_id=3,
            name='Meeting 3',
            committee_id=committee1.id,
            start_time=datetime(2017, 3, 1, 0),
            end_time=datetime(2017, 3, 1, 1),
        )
        meeting4 = Meeting(
            id=4,
            short_id=None,
            name='Meeting 4',
            committee_id=committee1.id,
            start_time=datetime(2017, 4, 1, 0),
            end_time=datetime(2017, 4, 1, 1),
        )
        meeting5 = Meeting(
            id=5,
            short_id=None,
            name='General Meeting 5',
            start_time=datetime(2017, 5, 1, 0),
            end_time=datetime(2017, 5, 1, 1),
        )
        meeting6 = Meeting(
            id=6,
            short_id=None,
            name='General Meeting 6',
            start_time=datetime(2017, 6, 1, 0),
            end_time=datetime(2017, 6, 1, 1),
        )
        meeting7 = Meeting(
            id=7,
            short_id=7,
            name='General Meeting 7',
            start_time=datetime(2017, 7, 1, 0),
            end_time=datetime(2017, 7, 1, 1),
        )

        attendee2 = Attendee(
            meeting=meeting2,
            member=m
        )
        attendee3 = Attendee(
            meeting=meeting3,
            member=m
        )
        session.add_all([
            committee1,
            meeting1,
            attendee2,
            attendee3,
            meeting4,
            meeting5,
            meeting6,
            meeting7
        ])

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def test_create_meeting(self):
        name = 'New Meeting'
        landing_url = 'docs.google.com/my_landing_url'
        committee_id = 1
        code = 5678
        start_time = '2017-05-01T00:00:00+00:00'
        end_time = '2017-05-01T01:00:00+00:00'
        payload = {
            'name': name,
            'code': code,
            'committee_id': committee_id,
            'landing_url': landing_url,
            'start_time': start_time,
            'end_time': end_time
        }

        response = post_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'meeting': {
                'code': code,
                'committee_id': committee_id,
                'id': 8,
                'landing_url': landing_url,
                'name': 'New Meeting',
                'start_time': start_time,
                'end_time': end_time
            },
            'status': 'success'
        }

        session = Session()
        assert session.query(Meeting).count() == 8

    def test_create_meeting_duplicate(self):
        payload = {'name': 'Meeting 1'}
        response = post_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 400

        session = Session()
        assert session.query(Meeting).count() == 7

    def test_create_meeting_invalid(self):
        payload = {'name': 'New Meeting', 'code': 12345}
        response = post_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 400

        session = Session()
        assert session.query(Meeting).count() == 7

    def test_get_meetings(self):
        response = get_json(self.app, '/meeting/list')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == [
            {
                'id': 7,
                'name': 'General Meeting 7',
                'committee_id': None,
                'code': 7,
                'landing_url': None,
                'start_time': '2017-07-01T00:00:00+00:00',
                'end_time': '2017-07-01T01:00:00+00:00'
            },
            {
                'id': 6,
                'name': 'General Meeting 6',
                'committee_id': None,
                'code': None,
                'landing_url': None,
                'start_time': '2017-06-01T00:00:00+00:00',
                'end_time': '2017-06-01T01:00:00+00:00'
            },
            {
                'id': 5,
                'name': 'General Meeting 5',
                'committee_id': None,
                'code': None,
                'landing_url': None,
                'start_time': '2017-05-01T00:00:00+00:00',
                'end_time': '2017-05-01T01:00:00+00:00'
            },
            {
                'id': 4,
                'name': 'Meeting 4',
                'committee_id': 1,
                'code': None,
                'landing_url': None,
                'start_time': '2017-04-01T00:00:00+00:00',
                'end_time': '2017-04-01T01:00:00+00:00'
            },
            {
                'id': 3,
                'name': 'Meeting 3',
                'committee_id': 1,
                'code': 3,
                'landing_url': None,
                'start_time': '2017-03-01T00:00:00+00:00',
                'end_time': '2017-03-01T01:00:00+00:00'
            },
            {
                'id': 2,
                'name': 'Meeting 2',
                'committee_id': None,
                'code': 2,
                'landing_url': None,
                'start_time': '2017-02-01T00:00:00+00:00',
                'end_time': '2017-02-01T01:00:00+00:00'
            },
            {
                'id': 1,
                'name': 'Meeting 1',
                'committee_id': None,
                'code': 1,
                'landing_url': None,
                'start_time': '2017-01-01T00:00:00+00:00',
                'end_time': '2017-01-01T01:00:00+00:00'
            },
        ]

    def test_update_meeting_landing_url(self):
        landing_url = 'docs.google.com/my_landing_url'
        payload = {
            'meeting_id': 2,
            'landing_url': landing_url,
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['landing_url'] == landing_url

    def test_update_meeting_landing_url_not_found(self):
        payload = {
            'meeting_id': 8398,
            'landing_url': 'docs.google.com/my_landing_url',
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 404

    def test_update_meeting_code(self):
        payload = {
            'meeting_id': 3,
            'code': '1234'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['code'] == 1234

    def test_unset_meeting_code(self):
        payload = {
            'meeting_id': 3,
            'code': None
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['code'] is None

    def test_update_meeting_code_autogenerate(self):
        payload = {
            'meeting_id': 4,
            'code': 'autogenerate'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert 1000 <= result['meeting']['code'] <= 9999

    def test_update_meeting_code_autogenerate_existing_code(self):
        payload = {
            'meeting_id': 3,
            'code': 'autogenerate'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert 1000 <= result['meeting']['code'] <= 9999

    def test_update_meeting_code_invalid_code(self):
        payload = {
            'meeting_id': 1,
            'code': '000F'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 400

    def test_update_meeting_null_code(self):
        payload = {
            'meeting_id': 1,
            'code': None
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

    def test_update_meeting_code_meeting_not_found(self):
        payload = {
            'meeting_id': 82,
            'code': '1234'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 404

    def test_update_meeting_times(self):
        payload = {
            'meeting_id': 4,
            'start_time': '2017-04-02T00:00:00+00:00',
            'end_time': '2017-04-02T01:00:00+00:00'
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['start_time'] == '2017-04-02T00:00:00+00:00'
        assert result['meeting']['end_time'] == '2017-04-02T01:00:00+00:00'

    def test_update_meeting_clear_values(self):
        payload = {
            'meeting_id': 4,
            'landing_url': None,
            'start_time': None,
            'end_time': None
        }
        response = patch_json(self.app, '/meeting', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result['status'] == 'success'
        assert result['meeting']['landing_url'] is None
        assert result['meeting']['start_time'] is None
        assert result['meeting']['end_time'] is None

    def test_attend_meeting(self):
        payload = {'meeting_short_id': 1}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success', 'landing_url': None}

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=1, member_id=1).count() == 1

    def test_attend_meeting_invalid_code(self):
        payload = {'meeting_short_id': 'a'}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 400

    def test_attend_meeting_duplicate(self):
        payload = {'meeting_short_id': 2}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 400

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=2, member_id=1).count() == 1

    def test_attend_meeting_as_admin_with_somebody_else(self):
        somebody_else = Member(id=2)
        session = Session()
        session.add(somebody_else)
        session.commit()

        payload = {'meeting_short_id': 1, 'member_id': somebody_else.id}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success', 'landing_url': None}

        assert session.query(Attendee).filter_by(meeting_id=1, member_id=1).count() == 0
        assert session.query(Attendee).filter_by(meeting_id=1, member_id=2).count() == 1

    @patch(
        'membership.services.eligibility.san_francisco_eligibility_service.get_current_time',
        return_value=datetime(2017, 7, 1, 1).replace(tzinfo=timezone.utc)
    )
    def test_attend_meeting_with_eligibility_at_attendance(self, get_current_time_mock):
        session = Session()
        member = session.query(Member).get(1)
        role = Role(member=member, role='member', committee_id=None)
        meeting5 = session.query(Meeting).get(5)
        meeting6 = session.query(Meeting).get(6)
        attendee5 = Attendee(member=member, meeting=meeting5)
        attendee6 = Attendee(member=member, meeting=meeting6)
        session.add_all([role, attendee5, attendee6])
        session.commit()

        payload = {'meeting_short_id': 7}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {'status': 'success', 'landing_url': None}

        attendee = session.query(Attendee).filter_by(meeting_id=7, member=member).one()
        assert attendee.eligible_to_vote is True

    def test_attend_meeting_meeting_not_found(self):
        payload = {'meeting_short_id': 1337}
        response = post_json(self.app, '/meeting/attend', payload=payload)
        assert response.status_code == 404

    def test_get_meeting(self):
        response = get_json(self.app, '/meetings/1')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            'id': 1,
            'name': 'Meeting 1',
            'committee_id': None,
            'code': 1,
            'landing_url': None,
            'start_time': '2017-01-01T00:00:00+00:00',
            'end_time': '2017-01-01T01:00:00+00:00'
        }

    def test_get_meeting_not_found(self):
        response = get_json(self.app, '/meetings/1337')
        assert response.status_code == 404

    def test_get_meeting_attendees(self):
        response = get_json(self.app, '/meetings/3/attendees')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == [
            {'id': 1, 'name': ''},
        ]

    def test_get_meeting_attendees_with_eligibility(self):
        session = Session()
        somebody_else = Member(id=2)
        attendee1 = Attendee(member_id=1, meeting_id=7, eligible_to_vote=True)
        attendee2 = Attendee(member_id=2, meeting_id=7)
        session.add_all([somebody_else, attendee1, attendee2])
        session.commit()

        response = get_json(self.app, '/meetings/7/attendees')
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == [
            {
                'id': 1,
                'name': '',
                'email': SUPER_USER_EMAIL,
                'eligibility': {'is_eligible': True, 'message': 'eligible', 'num_votes': 1}
            },
            {
                'id': 2,
                'name': '',
                'email': None,
                'eligibility': {'is_eligible': False, 'message': 'not eligible', 'num_votes': 0}
            },
        ]

    def test_get_meeting_attendees_meeting_not_found(self):
        response = get_json(self.app, '/meetings/1337/attendees')
        assert response.status_code == 404

    @patch(
        'membership.services.eligibility.san_francisco_eligibility_service.get_current_time',
        return_value=datetime(2017, 7, 1, 1).replace(tzinfo=timezone.utc)
    )
    def test_update_meeting_attendee_eligibility_at_attendance(self, get_current_time_mock):
        session = Session()
        member = session.query(Member).get(1)
        attendee = Attendee(member=member, meeting_id=7)
        session.add(attendee)

        assert attendee.eligible_to_vote is None

        role_member = Role(
            member=member,
            role='member',
            committee_id=None
        )
        role_active = Role(
            member=member,
            role='active',
            committee_id=1
        )
        session.add_all([role_member, role_active])
        session.commit()

        payload = {
            'update_eligibility_to_vote': True,
        }
        response = patch_json(self.app, '/meetings/7/attendees/1', payload=payload)
        assert response.status_code == 200

        attendee = session.query(Attendee).filter_by(meeting_id=7, member_id=1).one()

        assert attendee.eligible_to_vote is True

        session.delete(role_member)
        session.delete(role_active)
        session.commit()

        response = patch_json(self.app, '/meetings/7/attendees/1', payload=payload)
        assert response.status_code == 200

        assert attendee.eligible_to_vote is False

    def test_remove_meeting_attendee(self):
        response = delete_json(self.app, '/meetings/3/attendees/1')
        assert response.status_code == 200

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=3, member_id=1).count() == 0

    def test_remove_meeting_attendee_meeting_not_found(self):
        response = delete_json(self.app, '/meetings/1337/attendees/1')
        assert response.status_code == 404

    def test_add_attendee_from_kiosk(self):
        payload = {
            'email_address': SUPER_USER_EMAIL,
            'first_name': '',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=1, member_id=1).count() == 1

        # Adding this attendee should not have created a new member
        assert session.query(Member).count() == 1

    def test_add_attendee_from_kiosk_new_member(self):
        email_address = 'emma.goldman@riseup.net'
        payload = {
            'email_address': email_address,
            'first_name': 'Emma',
            'last_name': 'Goldman',
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        session = Session()
        member = session.query(Member).filter_by(email_address=email_address).one_or_none()
        assert member is not None
        assert member.first_name == 'Emma'
        assert member.last_name == 'Goldman'
        assert member.email_address == 'emma.goldman@riseup.net'
        assert member.normalized_email == 'emmagoldman@riseup.net'
        assert member.date_created is not None

        assert session.query(Attendee).filter_by(meeting_id=1, member_id=member.id).count() == 1

    def test_add_attendee_from_kiosk_duplicate_attendee(self):
        payload = {
            'email_address': SUPER_USER_EMAIL,
            'first_name': '',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 200

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=1, member_id=1).count() == 1

    def test_add_attendee_from_kiosk_no_email(self):
        payload = {
            'email_address': None,
            'first_name': '',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/1/attendee', payload=payload)
        assert response.status_code == 400

    def test_add_attendee_from_kiosk_meeting_not_found(self):
        payload = {
            'email_address': SUPER_USER_EMAIL,
            'first_name': '',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/1337/attendee', payload=payload)
        assert response.status_code == 404

    @patch(
        'membership.services.eligibility.san_francisco_eligibility_service.get_current_time',
        return_value=datetime(2017, 7, 1, 1).replace(tzinfo=timezone.utc)
    )
    def test_add_attendee_from_kiosk_with_eligibility_at_attendance(self, get_current_time_mock):
        session = Session()
        member = session.query(Member).get(1)
        role = Role(member=member, role='member', committee_id=None)
        meeting5 = session.query(Meeting).get(5)
        meeting6 = session.query(Meeting).get(6)
        attendee5 = Attendee(member=member, meeting=meeting5)
        attendee6 = Attendee(member=member, meeting=meeting6)
        session.add_all([role, attendee5, attendee6])
        session.commit()

        payload = {
            'email_address': SUPER_USER_EMAIL,
            'first_name': '',
            'last_name': '',
        }
        response = post_json(self.app, '/meetings/7/attendee', payload=payload)
        assert response.status_code == 200

        attendee = session.query(Attendee).filter_by(meeting_id=7, member=member).one()
        assert attendee.eligible_to_vote is True
