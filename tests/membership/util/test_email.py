from config import SUPER_USER_EMAIL, SUPER_USER_FIRST_NAME, SUPER_USER_LAST_NAME
from membership.database.models import Member
from membership.util.email import send_welcome_email


class TestEmail:

    def test_send_welcome_email_no_error(self):
        member = Member(
            first_name=SUPER_USER_FIRST_NAME,
            last_name=SUPER_USER_LAST_NAME,
            email_address=SUPER_USER_EMAIL,
            do_not_email=False,
        )
        send_welcome_email(member, 'https://dsasftest.org/')
