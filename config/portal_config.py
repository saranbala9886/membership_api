from config.lib import from_env

DEFAULT_CHAPTER_ID: int = from_env.get_int('DEFAULT_CHAPTER_ID', 1)
"""
A default chapter_id for all requests when one is not provided.

This is a temporary solution until the UI knows and communicates the chapter_id for each request.

When building out API endpoints, the controller should determine the chapter_id (or fallback to this
default) and pass this chapter_id as an explicit argument to the service.
"""

PORTAL_URL: str = from_env.get_str('PORTAL_URL', 'http://localhost:3000')
