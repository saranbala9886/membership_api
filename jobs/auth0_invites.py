#!/usr/bin/env python3


import argparse
import sys
import time
from typing import Dict, IO, List

from overrides import overrides

from jobs import Job
from membership.database.base import Session
from membership.database.models import Member
from membership.util.email import send_welcome_email
from membership.web.auth import create_auth0_user


class SendAuth0Invites(Job):
    """
    Sends Auth0 invites to members in the portal.
    Gracefully handles members who already have an Auth0 user.
    """

    @overrides
    def build_parser(self, parser: argparse.ArgumentParser) -> None:
        parser.add_argument('ids', nargs='+', type=int)
        parser.add_argument('-o', '--output', type=argparse.FileType('w'), default=sys.stdout)
        parser.add_argument('-f', '--force', action='store_true')

    @overrides
    def run(self, config: dict) -> None:
        outfile: IO[str] = config.get('output', sys.stdout)
        member_ids: List[int] = config.get('ids')

        self.send_invites(member_ids, out=outfile)

    def send_invites(self, member_ids: List[int], out: IO[str] = sys.stdout) -> None:
        """
        Send Auth0 invites to a batch of members.
        :param member_ids: the list of member IDs
        :param out: an output stream to which to write
        """
        existing = []
        created = {}
        errors = {}
        session = Session()

        members = session.query(Member).filter(Member.id.in_(member_ids))

        for member in members:
            time.sleep(1)
            try:
                created[member.email_address] = self.send_invite(member, out)
            except Exception as ex:
                if 'The user already exists' in str(ex):
                    existing.append(member.email_address)
                else:
                    errors[member.email_address] = str(ex)

        print(f"Existing ({len(existing)}):", file=out)
        print("\n".join(existing), file=out)
        print(f"Created ({len(created)}):\n{created}", file=out)
        print(f"Errors ({len(errors)}):\n{errors}", file=out)

    def send_invite(self, member: Member, out: IO[str]) -> Dict[str, str]:
        """
        Send an Auth0 invite to a single member.
        :param member: the member to invite
        :param out: an output stream to which to write
        :return: whether the invite succeeded and a link to the validation URL
        """
        email = member.email_address
        link = create_auth0_user(email)
        print(f"User created for {email}", file=out)
        result = {'link': link}
        try:
            send_welcome_email(member, link)
            result['email'] = 'success'
            print(f"Email sent to {member.name} ({email})", file=out)
            return result
        except Exception:
            result['email'] = 'failed'
