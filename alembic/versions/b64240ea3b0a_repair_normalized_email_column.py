"""Repair normalized_email column.

Revision ID: b64240ea3b0a
Revises: d2ff1f5e4485
Create Date: 2019-08-27 05:22:26.174705

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql
from membership.database.base import Session
from membership.database.models import Member

# revision identifiers, used by Alembic.
revision = 'b64240ea3b0a'
down_revision = 'd2ff1f5e4485'
branch_labels = None
depends_on = None


def upgrade():
    session = Session(bind=op.get_bind())
    unnormalized = session.query(Member).filter(Member.normalized_email == None)

    for member in unnormalized:
        if member.email_address is not None:
            member.normalized_email = Member.normalize_email(member.email_address)
            session.add(member)
            session.commit()

    # Final commit to cover the case where `unnormalized` is empty.
    session.commit()


def downgrade():
    pass
