"""add super user

Revision ID: 51cddc530569
Revises: 9d6648e02bfb
Create Date: 2019-06-11 22:58:56.125696

"""
import sqlalchemy as sa
from alembic import op

from config import SUPER_USER_FIRST_NAME, SUPER_USER_LAST_NAME, SUPER_USER_EMAIL, SUPER_USER_CHAPTER_NAME
from membership.database.models import Member
from membership.util.time import get_current_time

# revision identifiers, used by Alembic.
revision = '51cddc530569'
down_revision = '9d6648e02bfb'
branch_labels = None
depends_on = None

# Create an ad-hoc tables to use for the insert and delete statements.
chapter_table = sa.table(
    'chapters',
    sa.Column('id', sa.Integer),
    sa.Column('name', sa.String),
)

member_table = sa.table(
    'members',
    sa.Column('id', sa.Integer),
    sa.Column('first_name', sa.String),
    sa.Column('last_name', sa.String),
    sa.Column('biography', sa.String),
    sa.Column('email_address', sa.String),
    sa.Column('normalized_email', sa.String),
    sa.Column('do_not_call', sa.Boolean),
    sa.Column('do_not_email', sa.Boolean),
    sa.Column('date_created', sa.DateTime),
)

role_table = sa.table(
    'roles',
    sa.Column('member_id', sa.Integer),
    sa.Column('chapter_id', sa.Integer),
    sa.Column('role', sa.String),
    sa.Column('date_created', sa.DateTime),
)


def upgrade():
    op.bulk_insert(chapter_table, [
        {'id': 1, 'name': SUPER_USER_CHAPTER_NAME}
    ])
    op.bulk_insert(member_table, [
        {
            'id': 1,
            'first_name': SUPER_USER_FIRST_NAME,
            'last_name': SUPER_USER_LAST_NAME,
            'biography': """Admin user created by the initial migration script""",
            'email_address': SUPER_USER_EMAIL,
            'normalized_email': Member.normalize_email(SUPER_USER_EMAIL),
            'do_not_call': False,
            'do_not_email': False,
            'date_created': get_current_time(),
        }
    ])
    op.bulk_insert(role_table, [
        {'member_id': 1, 'chapter_id': 1, 'role': 'member', 'date_created': get_current_time()},
        {'member_id': 1, 'chapter_id': 1, 'role': 'admin', 'date_created': get_current_time()},
    ])


def downgrade():
    op.execute(
        role_table.delete().where(role_table.c.member_id == 1)
    )
    op.execute(
        member_table.delete().where(member_table.c.id == 1)
    )
    op.execute(
        chapter_table.delete().where(chapter_table.c.id == 1)
    )
