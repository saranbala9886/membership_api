import logging

from config import LOGGING_ROOT_LEVEL
from membership.web.base_app import app

if __name__ == '__main__':
    logging.getLogger().setLevel(LOGGING_ROOT_LEVEL)
    app.run(host='0.0.0.0', threaded=True, port=8080)
