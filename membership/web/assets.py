import boto3
from flask import Blueprint, jsonify, request
from jsonschema import ValidationError, validate

from membership.models import Asset, AuthContext
from membership.schemas.rest.assets import format_resolved_asset
from membership.services.asset_service import FileSystemAssetResolver
from membership.services \
    import AssetService, AssetLabelService, S3AssetResolver, DuplicateAssetPathError
from membership.web.auth import requires_auth
from membership.web.util import BadRequest
import config

import logging

logger = logging.getLogger(__file__)

asset_api = Blueprint('asset_api', __name__)
asset_tag_service = AssetLabelService()

if config.ASSETS_ENGINE == 's3':
    s3_client = boto3.client('s3')
    asset_resolver = S3AssetResolver(
        client=s3_client,
        bucket=config.ASSETS_BUCKET,
        path_prefix=config.ASSETS_PATH_PREFIX,
        region=config.ASSETS_BUCKET_REGION
    )
else:
    import os
    dirname = os.path.dirname(__file__)
    asset_resolver = FileSystemAssetResolver(
        os.path.join(dirname, 'static'), config.ASSETS_PATH_PREFIX)
asset_service = AssetService(asset_tag_service, asset_resolver)


@asset_api.route('/assets', methods=['GET'])
@requires_auth()
def get_assets(ctx: AuthContext):
    filter_ids_query = \
        request.args.get('id') or request.args.get('ids') or request.args.get('filter_ids')
    filter_ids = set(filter_ids_query.split(',')) if filter_ids_query else None
    filter_labels_query = \
        request.args.get('label') or request.args.get('labels') or request.args.get('filter_labels')
    filter_labels = set(filter_labels_query.split(',')) if filter_labels_query else None
    assets = asset_service.search(
        ctx.session,
        filter_labels=filter_labels,
        filter_ids=filter_ids,
    )
    resolved_assets = (asset_service.resolve(asset) for asset in assets)
    return jsonify({
        'status': 'success',
        'data': [format_resolved_asset(asset) for asset in resolved_assets]
    })


@asset_api.route('/assets/upload_url', methods=['GET'])
@requires_auth('admin')
def get_upload_url(ctx: AuthContext):
    relative_path = request.args.get('relative_path')
    if not relative_path:
        return BadRequest('Missing "relative_path" query parameter')
    url = asset_resolver.get_upload_url(relative_path)
    return jsonify({
        'status': 'success',
        'data': {
            'url': url
        }
    })


@asset_api.route('/assets/upload', methods=['PUT'])
@requires_auth('admin')
def upload_asset(ctx: AuthContext):
    relative_path = request.args.get('relative_path')
    if not relative_path:
        return BadRequest('Missing "relative_path" query parameter')
    asset_resolver.save(relative_path, request.stream)
    return jsonify({
        'status': 'success'
    })


@asset_api.route('/assets', methods=['PUT', 'POST'])
@requires_auth('admin')
def save_asset(ctx: AuthContext):
    body = request.json
    try:
        validate(body, {
            'relative_path': 'required',
            'content_type': 'required',
        })
    except ValidationError as ve:
        return BadRequest(ve.message)

    relative_path = body.get('relative_path')
    if not relative_path:
        return BadRequest(f"'relative_path' cannot be empty.")
    asset: Asset = asset_service.find_by_relative_path(ctx.session, relative_path) or Asset()
    if asset.relative_path is None:  # set path if new object
        asset.relative_path = relative_path
    labels_list = body.get('labels')
    labels = set(str(lbl) for lbl in labels_list) if labels_list else None
    asset.relative_path = body.get('relative_path')
    asset.content_type = body.get('content_type')

    try:
        asset_service.update(ctx.session, asset, labels)
    except DuplicateAssetPathError as err:
        return BadRequest(err.msg)

    return jsonify({
        'status': 'success',
        'data': format_resolved_asset(asset_service.resolve(asset))
    })


@asset_api.route('/asset/<int:asset_id>', methods=['DELETE'])
@requires_auth('admin')
def delete_asset(ctx: AuthContext, asset_id: int):
    asset_service.delete_by_id(ctx.session, asset_id)
    return jsonify({
        'status': 'success'
    })
