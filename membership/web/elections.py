from typing import Optional, Tuple

from flask import Blueprint, jsonify, request, Response

import logging
import random

from membership.database.base import Session
from membership.database.models import AuthToken, Candidate, Election, EligibleVoter, Ranking, \
    Sponsor, Vote
from membership.models import AuthContext
from membership.schemas.rest.elections import format_election, format_election_details
from membership.services import ElectionService, ElectionStateMachine
from membership.util.vote import STVElection
from membership.web.auth import requires_auth
from membership.web.util import BadRequest, NotFound, ServerError, Ok
from membership.web.submit_vote_handler import handle_vote, VotingError
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import joinedload

election_api = Blueprint('election_api', __name__)
election_service = ElectionService()


@election_api.route('/election/list', methods=['GET'])
@requires_auth()
def get_elections(ctx: AuthContext):
    elections = election_service.list_elections(ctx.session)
    result = [format_election(election) for election in elections]
    return jsonify(result)


@election_api.route('/election', methods=['GET'])
@requires_auth()
def get_election_by_id(ctx: AuthContext):
    election_id = request.args['id']
    election = election_service.find_election_by_id(ctx.session, election_id)
    if election is None:
        return NotFound('Election id={} does not exist'.format(election_id))
    else:
        return jsonify(format_election_details(election))


@election_api.route('/election', methods=['POST'])
@requires_auth('admin')
def add_election(ctx: AuthContext):
    election = Election(
        name=request.json['name'],
        number_winners=int(request.json['number_winners']),
        description=request.json.get('description'),
        description_img=request.json.get('description_img'),
        author=request.json.get('author')
    )

    election_service.set_times(election, request.json)

    if ctx.session.query(Election).filter_by(name=election.name).count() > 0:
        return BadRequest('Election already exists.')
    ctx.session.add(election)

    candidate_names = request.json['candidate_list']
    for candidate_name in candidate_names:
        candidate_name = candidate_name.strip()
        if candidate_name:
            candidate = Candidate(election=election, name=candidate_name)
            ctx.session.add(candidate)

    if request.json.get('sponsor_list'):
        sponsors = request.json['sponsor_list']
        for name in sponsors:
            sponsor = Sponsor(election=election, name=name)
            ctx.session.add(sponsor)

    ctx.session.commit()
    return jsonify({'status': 'success'})


def _verify_election_in_editable_state(
    ctx: AuthContext,
    election_id,
) -> Tuple[Optional[Election], Optional[Response]]:
    election = election_service.find_election_by_id(ctx.session, election_id)

    if election is None:
        return election, NotFound('Election id={} does not exist'.format(election_id))
    if election.status != ElectionStateMachine.DRAFT:
        return election, BadRequest('Election must be a draft')

    return election, None


@election_api.route('/election/<int:election_id>/candidates', methods=['POST'])
@requires_auth('admin')
def add_candidate(ctx: AuthContext, election_id: int):
    election, election_verification = _verify_election_in_editable_state(ctx, election_id)
    if election_verification:
        return election_verification
    name = request.json['name'].strip()
    if not name:
        return BadRequest(f'Candidate name not provided')
    image_url = request.json.get('image_url')
    candidate = Candidate(election_id=election_id, name=name, image_url=image_url)
    ctx.session.add(candidate)
    ctx.session.commit()
    return Ok({
        'id': candidate.id,
        'name': candidate.name,
        'image_url': candidate.image_url,
    })


@election_api.route('/election/<int:election_id>/candidates/<int:candidate_id>', methods=['PATCH'])
@requires_auth('admin')
def edit_candidate(ctx: AuthContext, election_id: int, candidate_id: int):
    election, election_verification = _verify_election_in_editable_state(ctx, election_id)
    if election_verification:
        return election_verification
    candidate = ctx.session.query(Candidate).filter_by(id=candidate_id).first()
    if not candidate:
        return NotFound(f'Candidate {candidate_id} not found')
    name = request.json.get('name')
    if name:
        candidate.name = name
    image_url = request.json.get('image_url')
    if image_url:
        candidate.image_url = image_url
    ctx.session.add(candidate)
    ctx.session.commit()
    return Ok({
        'id': candidate.id,
        'name': candidate.name,
        'image_url': candidate.image_url,
    })


@election_api.route('/election/<int:election_id>/candidates/<int:candidate_id>', methods=['DELETE'])
@requires_auth('admin')
def delete_candidate(ctx: AuthContext, election_id: int, candidate_id: int):
    election, election_verification = _verify_election_in_editable_state(ctx, election_id)
    if election_verification:
        return election_verification
    candidate = ctx.session.query(Candidate).filter_by(id=candidate_id).first()
    if not candidate:
        return NotFound(f'Candidate {candidate_id} not found')
    ctx.session.delete(candidate)
    ctx.session.commit()
    return Ok()


@election_api.route('/election/<int:election_id>/state/<state_transition>', methods=['PUT'])
@requires_auth('admin')
def change_election_state(ctx: AuthContext,
                          election_id: int,
                          state_transition: str):
    (result, election) = election_service.transition_election_as(
        ctx.session,
        state_transition,
        election_id)

    if not result:
        if election is None:
            return NotFound('Election id={} does not exist'.format(election_id))
        else:
            return BadRequest('Invalid state transition "{}"'.format(state_transition))

    return jsonify({
        'status': 'success',
        'election_status': election.status,
        'transitions': election_service.next_election_transitions(election),
    })


@election_api.route('/election/<int:election_id>', methods=['PATCH'])
@requires_auth('admin')
def update_election(ctx: AuthContext, election_id: int):
    election, election_verification = _verify_election_in_editable_state(ctx, election_id)
    if election_verification:
        return election_verification

    election_service.set_times(election, request.json)
    error, _ = election_service.set_number_of_winners(election, request.json)
    if error:
        return error

    ctx.session.add(election)
    ctx.session.commit()

    return jsonify({
        'status': 'success',
        'election': format_election_details(election)
    })


@election_api.route('/election/<int:election_id>/generate_token/<int:member_id>', methods=['GET'])
@requires_auth('admin')
def generate_election_token(ctx: AuthContext,
                            election_id: int,
                            member_id: int):

    auth_token = AuthToken(
        member_id=member_id,
        path='/vote',
        request_json_params='{"election_id":%d}' % election_id,
        single_use=True,
    )
    ctx.session.add(auth_token)
    ctx.session.flush()

    return jsonify({
        'auth_token_id': auth_token.id,
    })


@election_api.route('/election/eligible/list', methods=['GET'])
@requires_auth('admin')
def get_eligible(ctx: AuthContext):
    election_id = request.args['election_id']
    if election_service.find_election_by_id(ctx.session, election_id) is None:
        return NotFound('Election id={} does not exist'.format(election_id))

    eligibles = ctx.session.query(EligibleVoter) \
        .filter_by(election_id=election_id).options(joinedload(EligibleVoter.member)).all()
    return jsonify({
        eligible.member_id: {
            'name': eligible.member.name,
            'email_address': eligible.member.email_address
        } for eligible in eligibles
    })


@election_api.route('/election/<int:election_id>/vote/<int:ballot_key>', methods=['GET'])
@requires_auth()
def get_vote(ctx: AuthContext, election_id: int, ballot_key: int):
    vote = ctx.session.query(Vote) \
        .filter(Vote.election_id == election_id, Vote.vote_key == ballot_key) \
        .one_or_none()
    if not vote:
        return NotFound('Ballot with vote_key={} does not exist for election_id={}'.format(
            ballot_key,
            election_id))
    else:
        rankings = [ranking.candidate_id for ranking in vote.ranking]
        return jsonify({
            'election_id': election_id,
            'ballot_key': ballot_key,
            'rankings': rankings
        })


@election_api.route('/ballot/issue', methods=['POST'])
@requires_auth('admin')
def issue_ballot(ctx: AuthContext):
    election_id = request.json.get('election_id')
    if election_id is None:
        return BadRequest('Missing param "election_id"')

    member_id = request.json.get('member_id')
    if member_id is None:
        return BadRequest('Missing param "member_id"')

    eligible = ctx.session.query(EligibleVoter). \
        filter_by(member_id=member_id, election_id=election_id).with_for_update().one_or_none()
    if not eligible:
        return BadRequest('Voter is not eligible for this election.')
    if eligible.voted:
        return BadRequest('Voter has either already voted or received a paper ballot for this '
                          'election.')
    eligible.voted = True
    ctx.session.commit()
    return jsonify({'status': 'success'})


@election_api.route('/ballot/claim', methods=['POST'])
@requires_auth('admin')
def add_paper_ballots(ctx: AuthContext):
    election_id = request.json.get('election_id')
    if election_id is None:
        return BadRequest('Missing param "election_id"')

    number_ballots = request.json.get('number_ballots')
    if number_ballots is None:
        return BadRequest('Missing param "number_ballots"')

    ballot_keys = []
    for i in range(0, number_ballots):
        vote, _ = create_vote(ctx.session, election_id, 5)
        ballot_keys.append(vote.vote_key)
    return jsonify(ballot_keys)


@election_api.route('/vote/paper', methods=['POST'])
@requires_auth('admin')
def submit_paper_vote(ctx: AuthContext):
    election_id = request.json['election_id']
    election = election_service.find_election_by_id(ctx.session, election_id)
    if election is None:
        return NotFound('Election id={} does not exist'.format(election_id))
    if election.status == 'final':
        return BadRequest('You may not submit more votes after an election has been marked final')
    vote_key = request.json['ballot_key']
    vote = ctx.session.query(Vote).filter_by(
        election_id=election_id,
        vote_key=vote_key,
    ).with_for_update().one_or_none()

    if not vote:
        return NotFound(
            f'Ballot with vote_key={vote_key} for election_id={election_id} not claimed'
        )

    override = request.json.get('override', False)
    rankings = request.json.get('rankings', [])
    if vote.ranking and not override:
        existing_rankings = [ranking.candidate_id for ranking in vote.ranking]
        if rankings == existing_rankings:
            return jsonify({'status': 'match'})
        else:
            return jsonify({'status': 'mismatch'})
    if override:
        for rank in vote.ranking:
            ctx.session.delete(rank)
    for rank, candidate_id in enumerate(rankings):
        ranking = Ranking(rank=rank, candidate_id=candidate_id)
        vote.ranking.append(ranking)
    ctx.session.add(vote)
    ctx.session.commit()
    return jsonify({'status': 'new'})


@election_api.route('/vote', methods=['POST'])
@requires_auth()
def submit_vote(ctx: AuthContext):
    election_id = request.json.get('election_id')
    if election_id is None:
        return BadRequest('Missing param "election_id"')

    rankings = request.json.get('rankings', [])
    maybe_vote_key = handle_vote(requester=ctx.requester, session=ctx.session,
                                 election_id=election_id, rankings=rankings)
    if type(maybe_vote_key) is not VotingError:
        return jsonify({'ballot_id': maybe_vote_key})

    if maybe_vote_key == VotingError.ALREADY_VOTED:
        return BadRequest('You have already voted in this election')
    if maybe_vote_key == VotingError.INELIGIBLE:
        return BadRequest('Not eligible to vote in this election')
    if maybe_vote_key == VotingError.TOO_LATE:
        return BadRequest('You may not submit a vote after the polls have closed')
    if maybe_vote_key == VotingError.TOO_EARLY:
        return BadRequest('Voting is not yet open')
    if maybe_vote_key == VotingError.UNKNOWN_ELECTION:
        return NotFound('Election id={} does not exist'.format(election_id))
    if maybe_vote_key == VotingError.VOTE_KEY_RESERVATION_FAILURE:
        return ServerError('Could not reserve a random vote key. Please try again.')


@election_api.route('/election/voter', methods=['POST'])
@requires_auth('admin')
def add_voter(ctx: AuthContext):
    member_id = request.json.get('member_id', ctx.requester.id)
    election_id = request.json.get('election_id')
    if election_id is None:
        return BadRequest('Missing param "election_id"')

    existing_voter = ctx.session.query(EligibleVoter)\
        .filter_by(member_id=member_id, election_id=election_id).one_or_none()

    if existing_voter is None:
        eligible_voter = EligibleVoter(member_id=member_id, election_id=election_id, voted=False)
        ctx.session.add(eligible_voter)
        ctx.session.commit()

    return jsonify({'status': 'success'})


@election_api.route('/election/count', methods=['GET'])
@requires_auth('admin')
def election_count(ctx: AuthContext):
    election_id = request.args['id']
    election = election_service.find_election_by_id(ctx.session, election_id)
    if election is None:
        return NotFound('Election id={} does not exist'.format(election_id))

    stv = hold_election(election)
    is_final = election.status == 'final'

    candidates = {
        candidate.id: {
            'name': candidate.name,
            'image_url': candidate.image_url,
        } for candidate in election.candidates
    }
    winners = [
        {
            'candidate': winner.candidate,
            'round': winner.round,
        } for winner in stv.winners if is_final
    ]
    round_information = [
        {
            'transfer_type': round.transfer_type.value,
            'transfer_source': round.transfer_source,
            'votes': {
                cid: {
                    'starting_votes': float(vote_count.starting),
                    'votes_received': float(vote_count.received),
                } for cid, vote_count in round.vote_counts.items()
            }
        } for round in stv.rounds if is_final
    ]

    return jsonify({
        'ballot_count': len(stv.votes),
        'quota': stv.quota,
        'candidates': candidates,
        'winners': winners,
        'round_information': round_information
    })


def hold_election(election: Election):
    votes = [
        [v.candidate_id for v in vote.ranking]
        for vote in election.votes if vote.ranking
    ]
    candidate_ids = [c.id for c in election.candidates]
    logging.info(
        "CANDIDATE_IDS: {}, VOTES: {}, NUM WINNERS: {}".format(
            candidate_ids, votes, election.number_winners
        )
    )
    stv = STVElection(candidate_ids, election.number_winners, votes, election.id)
    stv.hold_election()
    return stv


def create_vote(session: Session, election_id: int, digits: int):
    i = 0
    rolled_back = False
    while i < 5:
        try:
            a = random.randint(10 ** (digits - 1), 10 ** digits - 1)
            v = Vote(vote_key=a, election_id=election_id)
            session.add(v)
            session.commit()
            return v, rolled_back
        except IntegrityError:
            print('Had to retry')
            i += 1
            session.rollback()
            rolled_back = True
    raise Exception('Failing to find a random key in five tries. Think something is wrong.')
