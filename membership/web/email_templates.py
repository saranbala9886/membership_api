from flask import Blueprint, Response, request

from membership.database.models import EmailTemplate
from membership.models import AuthContext
from membership.schemas.rest import EmailTemplateRest
from membership.services import EmailTemplateService
from membership.web.auth import requires_auth
from membership.web.util import NotFound, requires_json, Ok

email_templates_api = Blueprint('email_templates_api', __name__)
email_template_service = EmailTemplateService


@email_templates_api.route('/email_templates', methods=['GET'])
@requires_auth('admin')
def list_email_templates(ctx: AuthContext) -> Response:
    return Ok([
        EmailTemplateRest.format(template)
        for template in ctx.session.query(EmailTemplate).all()
    ])


@email_templates_api.route('/email_template/<int:template_id>', methods=['GET'])
@requires_auth('admin')
def find_email_template(ctx: AuthContext, template_id: int) -> Response:
    found_template = ctx.session.query(EmailTemplate).get(template_id)
    if not found_template:
        return NotFound()
    else:
        return Ok(EmailTemplateRest.format(found_template))


@email_templates_api.route('/email_template/<int:template_id>', methods=['PUT'])
@requires_auth('admin')
@requires_json
def update_email_template(ctx: AuthContext, template_id: int) -> Response:
    found_template = ctx.session.query(EmailTemplate).get(template_id)
    template = email_template_service.upsert(ctx, request.json, found_template)
    if template is None:
        return NotFound()
    else:
        return Ok(EmailTemplateRest.format(template))


@email_templates_api.route('/email_template/<int:template_id>', methods=['DELETE'])
@requires_auth('admin')
def delete_email_template(ctx: AuthContext, template_id: int) -> Response:
    return Ok() if email_template_service.delete(ctx, template_id) else NotFound()


@email_templates_api.route('/email_template', methods=['POST'])
@requires_auth('admin')
@requires_json
def create_email_template(ctx: AuthContext) -> Response:
    template = email_template_service.upsert(ctx, request.json)
    if template is None:
        return NotFound()
    else:
        return Ok(EmailTemplateRest.format(template))
