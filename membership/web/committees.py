import logging
from flask import Blueprint, jsonify, request
from membership.database.models import Committee, Member, Role, Email
from membership.models import AuthContext
from membership.schemas.rest.committees import format_committee, format_committee_details
from membership.util.email import send_committee_request_email
from membership.web.auth import requires_auth
from membership.web.util import BadRequest, NotFound


committee_api = Blueprint('committee_api', __name__)


@committee_api.route('/committee/list', methods=['GET'])
@requires_auth()
def get_committees(ctx: AuthContext):
    committees = ctx.session.query(Committee).all()
    result = [format_committee(c, ctx.az) for c in committees]
    return jsonify(result)


@committee_api.route('/committee', methods=['POST'])
@requires_auth('admin')
def add_committee(ctx: AuthContext):
    committee = Committee(name=request.json['name'])
    ctx.session.add(committee)
    committee.provisional = request.json.get('provisional', False)
    admins = request.json.get('admin_list', [])
    members = ctx.session.query(Member).filter(Member.email_address.in_(admins)).all()
    for member in members:
        role = Role(role='admin', committee=committee, member=member)
        ctx.session.add(role)
    ctx.session.commit()
    return jsonify({
        'status': 'success',
        'created': format_committee_details(committee, ctx.az)
    })


@committee_api.route('/committee/<int:committee_id>', methods=['GET'])
@requires_auth()
def get_committee(ctx: AuthContext, committee_id: int):
    ctx.az.verify_admin(committee_id)
    committee = ctx.session.query(Committee).get(committee_id)
    if committee is None:
        return NotFound('Committee id={} does not exist'.format(committee_id))
    return jsonify(format_committee_details(committee, ctx.az))


@committee_api.route('/committee/<int:committee_id>/member_request', methods=['POST'])
@requires_auth()
def request_committee_membership(ctx: AuthContext, committee_id: int):
    name = ctx.requester.name
    member_email = ctx.requester.email_address
    email = ctx.session.query(Email).filter_by(committee_id=committee_id).one_or_none()
    if email is None or email.email_address is None:
        return BadRequest('There is no email address associated with this committee')

    email_sent = False
    try:
        send_committee_request_email(name, member_email, email.email_address)
        email_sent = True
    except Exception as e:
        logging.error(
            f"Could not send committee request email to committee.id={committee_id}: {e}")

    return jsonify(
        {
            'status': 'success',
            'data': {
                'email_sent': email_sent
            }
        }
    )
