from flask import Flask, jsonify
from flask_cors import CORS
from raven.contrib.flask import Sentry

from membership.web.util import CustomEncoder


# NOTE: Don't import from the membership package outside of this init function!
#
# Many parts of the code will reference "app" for the logger and this will cause
# cyclic dependencies of this file and the app. By putting all membership imports
# in this function, we can be sure that the app will initialize before anything
# references it.

def init(app: Flask):
    """
    Initializes the app after it has been constructed.

    Note: This is where you can import from the membership package
    """
    app.logger.info('Initializing app...')
    from membership.web.assets import asset_api
    from membership.web.members import member_api
    from membership.web.meetings import meeting_api
    from membership.web.elections import election_api
    from membership.web.email import email_api
    from membership.web.email_templates import email_templates_api
    from membership.web.email_topics import email_topics_api
    from membership.web.committees import committee_api
    CORS(app)
    app.register_blueprint(asset_api)
    app.register_blueprint(member_api)
    app.register_blueprint(meeting_api)
    app.register_blueprint(election_api)
    app.register_blueprint(email_api)
    app.register_blueprint(email_templates_api)
    app.register_blueprint(email_topics_api)
    app.register_blueprint(committee_api)
    app.logger.info('App initialization complete.')


app = Flask(__name__)
app.json_encoder = CustomEncoder
sentry = Sentry(app)
init(app)


@app.route('/health', methods=["GET"])
def health_check():
    return jsonify({'health': True})
