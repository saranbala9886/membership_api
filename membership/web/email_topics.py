from flask import Blueprint, Response, request
import logging

from sqlalchemy.exc import IntegrityError

from config import EMAIL_TOPICS_ADD_MEMBER_TOKEN
from membership.database.base import Session
from membership.database.models import EmailTemplate, Member, Meeting
from membership.models import AuthContext
from membership.models import NoAuthContext
from membership.schemas.rest import EmailTopicsRest
from membership.services import AttendeeService, InterestTopicsService, MemberService, \
    EmailTemplateService
from membership.services.eligibility import SanFranciscoEligibilityService
from membership.repos import MeetingRepo
from membership.util.email import send_member_emails
from membership.web.auth import requires_auth
from membership.web.util import BadRequest
from membership.web.util import NotFound
from membership.web.util import Ok
from membership.web.util import Unauthorized
from membership.web.util import requires_json


logger = logging.getLogger(__name__)
email_topics_api = Blueprint('email_topics_api', __name__)
interest_topics_service = InterestTopicsService
meeting_repository = MeetingRepo(Meeting)
attendee_service = AttendeeService()
eligibility_service = SanFranciscoEligibilityService(meeting_repository, attendee_service)
member_service = MemberService(eligibility_service)

email_template_service = EmailTemplateService


@email_topics_api.route('/email_topics', methods=['GET'])
@requires_auth('admin')
def list_email_topics(ctx: AuthContext) -> Response:
    return Ok([
        EmailTopicsRest.format(topic)
        for topic in interest_topics_service.list_all_interest_topics(ctx)
    ])


@email_topics_api.route('/email_topics/member_count/<topic_name>',
                        methods=['GET'])
@requires_auth('admin')
def member_count(ctx: AuthContext, topic_name) -> Response:
    topic: Interest = interest_topics_service \
        .find_interest_topic_by_name(ctx, topic_name)
    if topic is None:
        return NotFound('Topic {} does not exist'.format(topic_name))

    members = interest_topics_service.list_interest_topic_members(ctx, topic)
    return Ok(len(members))


@email_topics_api.route('/email_topics/add_member', methods=['POST'])
def add_member_to_topics() -> Response:
    logger.info('POST /email_topics/add_member')
    user_input = request.form

    email_address: str = user_input.get('email', '').strip()
    first_name: str = user_input.get('first_name', '').strip()
    last_name: str = user_input.get('last_name', '').strip()
    token: str = user_input.get('token', '')
    topics_str: str = user_input.get('topics', '')

    if token != EMAIL_TOPICS_ADD_MEMBER_TOKEN:
        return Unauthorized('Missing or invalid token')

    if email_address is None or len(email_address) == 0:
        return BadRequest('Missing required parameter "email"')

    ctx = NoAuthContext(Session())

    member = member_service.find_by_email(email_address, ctx.session)
    if member is None:
        # TODO: Centralize member creation in MemberService (this code is duplicated in members.py)
        member = Member(first_name=first_name, last_name=last_name, email_address=email_address)
        ctx.session.add(member)
        try:
            ctx.session.flush()
        except IntegrityError:
            ctx.session.rollback()
            return BadRequest(f"Failed to create member with address: {email_address}")

    topics: List[str] = [t.strip() for t in topics_str.split(',') if t.strip() is not '']
    if len(topics) > 0:
        logger.info(f'Saving member interests: {topics}')
        interest_topics_service.create_interests(ctx, member, topics)

    ctx.session.commit()
    return Ok()


@email_topics_api.route('/email_topics/send', methods=['POST'])
@requires_auth('admin')
@requires_json
def send_email_to_topic_members(ctx: AuthContext) -> Response:
    topic_str: Optional[str] = request.json.get('topic')
    topic_id_str: Optional[str] = request.json.get('topic_id')
    if not topic_id_str and not topic_str:
        return BadRequest('Missing parameter "topic_id"')
    topic_id: int = int(topic_id_str or topic_str)

    template_id: int = request.json.get('template_id')
    if template_id is None:
        return BadRequest('Missing parameter "template_id"')

    # TODO: validate sending email address?
    sending_address: str = request.json.get('sending_address')
    if sending_address is None:
        return BadRequest('Missing parameter "sending_address"')

    topic: InterestTopic = interest_topics_service.find_interest_topic(ctx, topic_id)
    if topic is None:
        return NotFound('Interest topic not found')

    template: EmailTemplate = ctx.session.query(EmailTemplate).get(template_id)
    if template is None:
        return NotFound('Email template not found')

    tag: str = f'{topic.name}_templates'

    members: List[str] = interest_topics_service.list_interest_topic_members(ctx, topic)

    recipient_variables = {member: {} for member in members}
    send_member_emails(sending_address,
                       template.subject,
                       template.body,
                       recipient_variables,
                       tag)

    return Ok()
