from typing import Optional

from membership.models import Asset, ResolvedAsset
from membership.schemas.rest.model import JsonObj


def format_asset(asset: Asset, upload_link: Optional[str] = None) -> JsonObj:
    upload_link_field = {'upload_link': upload_link} if upload_link else {}
    return {
        'id': asset.id,
        'content_type': asset.content_type,
        'relative_path': asset.relative_path,
        **upload_link_field
    }


def format_resolved_asset(resolved: ResolvedAsset) -> JsonObj:
    return {
        **format_asset(resolved.asset),
        'view_url': resolved.view_url
    }
