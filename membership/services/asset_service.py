import logging
import os
from typing import Iterable, Optional, Set

import boto3
import botocore.exceptions
from botocore.client import BaseClient
from flask import url_for
from overrides import overrides
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Query

from membership.database.base import Session
from membership.database.models import Asset, AssetLabel, Meeting
from membership.models import ResolvedAsset

logger = logging.getLogger(__name__)


class AssetResolver:

    def get_download_url(self, asset: Asset) -> str:
        return NotImplemented

    def get_upload_url(self, relative_path: str) -> str:
        return NotImplemented

    def save(self, relative_path: str, stream):
        pass

    def delete(self, relative_path: str) -> bool:
        return NotImplemented


class S3AssetResolver(AssetResolver):

    def __init__(
            self,
            client: BaseClient,
            bucket: str,
            path_prefix: str,
            region: str
    ):
        self.bucket = bucket
        self.path_prefix = path_prefix
        self.client = client
        self.region = region

    def get_full_path(self, relative_path: str):
        return f'{self.path_prefix}/{relative_path}'

    @overrides
    def get_download_url(self, asset: Asset) -> str:
        return self.client.generate_presigned_url(
            ClientMethod='get_object',
            Params={
                'Bucket': self.bucket,
                'Key': self.get_full_path(asset.relative_path)
            }
        )

    @overrides
    def get_upload_url(self, relative_path: str) -> str:
        url = self.client.generate_presigned_url(
            ClientMethod='put_object',
            Params={
                'Bucket': self.bucket,
                'Key': self.get_full_path(relative_path),
                'ContentType': 'application/octet-stream',
            }
        )
        # boto3 is not returning urls with the region included, causing
        # preflight OPTIONS requests to return a redirect, breaking CORS
        # for buckets not in us-east-1
        # https://github.com/boto/boto3/issues/421
        # https://github.com/boto/boto3/issues/1982
        if self.region != 'us-east-1':
            url = url.replace('.s3.', f".s3-{self.region}.")
        return url

    @overrides
    def delete(self, relative_path: str) -> bool:
        s3 = boto3.resource('s3')
        full_path = self.get_full_path(relative_path)
        try:
            s3.Object(self.bucket, full_path).delete()
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                # The object does not exist.
                return False
            else:
                # Something else has gone wrong.
                raise
        else:
            # The object does exist.
            return True


class FileSystemAssetResolver(AssetResolver):

    def __init__(self, static_dir: str, path_prefix: str):
        self.static_dir = static_dir
        self.path_prefix = path_prefix
        self.root_dir = os.path.join(static_dir, path_prefix)
        os.makedirs(self.root_dir, exist_ok=True)
        logging.info(f"root_dir={self.root_dir}")

    @overrides
    def get_download_url(self, asset: Asset) -> str:
        return url_for('static',
                       filename=os.path.join(self.path_prefix, asset.relative_path),
                       _external=True)

    @overrides
    def get_upload_url(self, relative_path: str) -> str:
        return url_for('asset_api.upload_asset', relative_path=relative_path, _external=True)

    @overrides
    def save(self, relative_path: str, stream):
        with open(os.path.join(self.root_dir, relative_path), "bw") as f:
            chunk_size = 4096
            while True:
                chunk = stream.read(chunk_size)
                if len(chunk) == 0:
                    return
                f.write(chunk)

    @overrides
    def delete(self, relative_path: str) -> bool:
        os.remove(os.path.join(self.root_dir, relative_path))
        return True


class AssetLabelService:

    def find_existing_labels(
            self,
            session: Session,
            filter_labels: Optional[Set[str]] = None
    ) -> Iterable[AssetLabel]:
        query = session.query(AssetLabel)
        if filter_labels is None:
            return query.all()
        else:
            return query.filter(AssetLabel.label.in_(filter_labels))

    def get_meeting_labels(self, meeting: Meeting) -> Set[str]:
        return {"meeting", f"meeting_id!{meeting.id}"}


class DuplicateAssetPathError(Exception):
    def __init__(self, asset_id: Optional[int], path: str, cause: Optional[Exception]):
        self.asset_id = asset_id
        self.path = path
        self.cause = cause
        self.msg = f"Duplicate path for Asset.id={asset_id} with name '{path}'"


class AssetService:

    def __init__(
            self,
            asset_labels: AssetLabelService,
            asset_resolver: AssetResolver
    ):
        self.asset_labels = asset_labels
        self.asset_resolver = asset_resolver

    def delete_by_id(self, session: Session, asset_id: int) -> None:
        asset = session.query(Asset).get(asset_id)
        if asset is not None:
            self.asset_resolver.delete(asset.relative_path)
            for lbl in asset.labels:
                session.delete(lbl)
            session.flush()
            session.delete(asset)
            session.commit()

    def find_by_id(self, session: Session, asset_id: int) -> Optional[Asset]:
        return session.query(Asset).get(asset_id)

    def find_by_relative_path(self, session: Session, path: str) -> Optional[Asset]:
        return session.query(Asset).filter(Asset.relative_path == path).one_or_none()

    def find_by_label(
            self,
            session: Session,
            filter_labels: Optional[Set[str]] = None
    ) -> Iterable[Asset]:
        return self.filter_by_label(session.query(Asset), filter_labels)

    def filter_by_id(self, query: Query, filter_ids: Optional[Set[int]]) -> Query:
        if filter_ids is None:
            return query
        return query.filter(Asset.id.in_(filter_ids))

    def filter_by_label(self, query: Query, filter_labels: Set[str]) -> Query:
        labels: Iterable[AssetLabel] = self.asset_labels.find_existing_labels(
            query.session,
            filter_labels
        )
        asset_ids = set([lbl.asset_id for lbl in labels])
        return query.filter(Asset.id.in_(asset_ids))

    def find_by_label_and_resolve(
            self,
            session: Session,
            filter_labels: Optional[Set[str]]) -> Iterable[ResolvedAsset]:
        results = self.find_by_label(session, filter_labels)
        return (self.resolve(asset) for asset in results)

    def search(
            self,
            session: Session,
            filter_ids: Optional[Set[int]] = None,
            filter_labels: Optional[Set[str]] = None
    ) -> Iterable[Asset]:
        filters = [
            lambda q: self.filter_by_label(q, filter_labels),
            lambda q: self.filter_by_id(q, filter_ids),
        ]
        query = session.query(Asset)
        for f in filters:
            query = f(query)
        return query

    def create(
            self,
            session: Session,
            path: str,
            content_type: str,
            labels: Optional[Set[str]] = None) -> Asset:
        asset = Asset(relative_path=path, content_type=content_type)
        try:
            session.add(asset)
            session.flush()
            labels_to_create = [AssetLabel(asset_id=asset.id, label=lbl) for lbl in (labels or {})]
            for label_to_create in labels_to_create:
                session.add(label_to_create)
            session.commit()
            return asset
        except IntegrityError as ex:
            logger.warning(f"IntegrityError updating assets table: {str(ex)}")
            session.rollback()
            raise DuplicateAssetPathError(asset.id, asset.relative_path, ex)

    def update(self, session: Session, asset: Asset, labels: Optional[Set[str]]) -> None:
        session.add(asset)
        if labels is not None:
            session.flush()
            asset_labels = asset.labels
            existing_labels = set([lbl.label for lbl in asset_labels])
            labels_to_remove = existing_labels - (labels or existing_labels)
            for lbl in asset_labels:
                if lbl.label in labels_to_remove:
                    session.delete(lbl)
            labels_to_add = labels - existing_labels
            for lbl in labels_to_add:
                session.add(AssetLabel(asset_id=asset.id, label=lbl))
        try:
            session.commit()
        except IntegrityError as ex:
            logger.warning(f"IntegrityError updating assets table: {str(ex)}")
            session.rollback()
            raise DuplicateAssetPathError(asset.id, asset.relative_path, ex)

    def resolve(self, asset: Asset) -> ResolvedAsset:
        view_url = self.asset_resolver.get_download_url(asset)
        return ResolvedAsset(asset, view_url)
