default: docker

# Run all tests in the container (same as CI)
test: lint
	docker-compose build test
	docker run dsasanfrancisco/membership_test

# Run quick unit tests without building the image
unittest:
	docker-compose run test -m 'not slow'

# Run unit tests and report coverage without building the image
unittest-cov:
	docker-compose run test --cov=membership --cov=jobs --cov-report=html

# Run code formatter
fmt:
	yapf . -r -i

# Run python linter
lint:
	docker-compose run --entrypoint=flake8 test

# Migrates to the loaded database
migrate: load
	docker-compose up migrate

# Generate a migration with the given message variable
migration:
	[ "$(MESSAGE)" != "" ] && docker-compose run migrate alembic revision --autogenerate -m "$(MESSAGE)" \
		|| echo "Usage: make migration MESSAGE='...'"

# Load and migrate all required infrastructure (databases, caches, etc)
load:
	docker-compose up -d db

# Start with docker
docker:
	docker-compose up -d
	docker-compose logs -f

# Start with docker, including web ui
all:
	docker-compose -f docker-compose.yml -f docker-compose.override.yml -f docker-compose.ui.yml up -d
	docker-compose logs -f

# Stop all docker containers, including web ui
stop:
	docker-compose -f docker-compose.yml -f docker-compose.override.yml -f docker-compose.ui.yml stop

# Build with docker
build:
	docker-compose build

# Clean up local files and one-time run containers
clean:
	find . | \
	grep -E "(__pycache__|\.pyc$$|\.sqlite$$)" | \
	xargs rm -rf && \
	docker-compose rm -f

# Remove all local code, stopped containers, and databases for this project
purge: clean
	rm -rf mnt

# Kill the running applications (docker-only)
kill:
	docker-compose kill

# In case you really need to clean house, but don't want to waste time spelling out all the commands
stalin: kill purge
	docker system prune

# All together now!
.PHONY: ci test unittest fmt lint debug migrate load docker build deploy clean purge kill stalin
